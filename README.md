# Project: Beyond Borders - Stays

### Description
Demo real estate listings app written in Vue, Firebase backend, Google Maps API.

[Check it out](https://beyondborders-stays.firebaseapp.com/#/)
