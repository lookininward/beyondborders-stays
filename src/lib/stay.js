import { Storage } from '@/firebase/storage';

export default class Stay {
  constructor (stay) {
    this.id = stay.id;
    this.address = stay.data().address;
    this.beds = stay.data().beds;
    this.baths = stay.data().baths;
    this.price = stay.data().price;
    this.description = stay.data().description;
    this.geopoint = stay.data().geopoint;
    this.coverImage = null;
    this.images = null;
    this.getCoverImage(stay);
    this.getImages(stay);
  }

  //-- Public Attributes ------------------------------------------------------
  async getCoverImage(stay) {
    let imageLocations = stay.data().images;
    let gsReference = Storage.refFromURL(imageLocations[0]);
    let url = await gsReference.getDownloadURL();
    this.coverImage = url;
  }

  /*
    Better to only download rest of images when use enters detail route
  */
  async getImages(stay) {
    let promises = stay.data().images.map(location =>
      Storage.refFromURL(location)
             .getDownloadURL()
             .then(url => url)
    );

    Promise.all(promises)
      .then(response => this.images = response);
  }
}
