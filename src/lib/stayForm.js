export default class StayForm {
  constructor () {
    this.values = {
      beds: 1,
      baths: 1,
      price: null,
      furnished: false,
      address: null,
      geopoint: null,
      description: null,
      images: [],
      gallery: []
    }

    this.errors = {
      beds: null,
      baths: null,
      price: null,
      furnished: null,
      address: null,
      geopoint: null,
      description: null,
      images: null
    }
  }

  //-- Public Attributes ------------------------------------------------------
  clearInput(input) {
    this.values[input] = null
  }

  clearErrors() {
    this.errors = {
      beds: null,
      baths: null,
      price: null,
      furnished: null,
      address: null,
      geopoint: null,
      description: null,
      images: null
    }
  }

  clearError(input) {
    this.errors[input] = null
  }

  validateForm() {
    const values = this.values

    // validate beds
    const beds = values.beds
    if (!beds) {
      this.errors['beds'] = 'missing'
    }

    // validate baths
    const baths = values.baths
    if (!baths) {
      this.errors['baths'] = 'missing'
    }

    // validate price
    const price = values.price
    if (!price) {
      this.errors['price'] = 'missing'
    }

    // validate furnished
    // const furnished = values.furnished
    // if (!furnished) {
    //   this.errors['furnished'] = 'missing'
    // }

    // validate address
    const address = values.address
    if (!address) {
      this.errors['address'] = 'missing'
    }

    // validate geopoint
    const geopoint = values.geopoint
    if (!geopoint) {
      this.errors['geopoint'] = 'missing'
    }

    // validate description
    // const description = values.description
    // if (!description) {
    //   this.errors['description'] = 'missing'
    // }

    // validate images
    // const images = values.images
    // if (!images.length) {
    //   this.errors['images'] = 'missing'
    // }

    // check if any errors set
    const errors = this.errors
    const hasErrors = Object.values(errors).some((e) => e)
    return !hasErrors
  }

  validateInput(input) {
    if (!this.values[input]) {
      this.errors[input] = 'missing'
    }
  }

  addImages(images) {
    if (!this.values.images.length) {
      images.forEach(image => {
        this.values.images.push(image)
      })
    }
  }

}
