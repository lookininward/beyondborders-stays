import Stays from '@/views/Stays.vue'
import Results from '@/views/Results.vue'
import Listing from '@/views/Listing.vue'
import Login from '@/views/Login.vue'
import SubmitStay from '@/views/SubmitStay.vue'
import CreateAccount from '@/views/CreateAccount.vue'
import ManageAccount from '@/views/ManageAccount.vue'
import Messages from '@/views/Messages.vue'

export default [{
  path: '/',
  component: Stays,

   children: [

   // Results
   {
    path: '',
    name: 'Results',
    component: Results,
    meta: { transitionName: 'slide' }
   },

   // Listing
    {
      path: '/:id',
      name: 'Listing',
      component: Listing,
      meta: { transitionName: 'slide' }
    },

    // Authentication
    {
      path: '/stays/login',
      name: 'login',
      component: Login,
      meta: { transitionName: 'slide' }
    },
    {
      path: '/stays/create-account',
      name: 'createAccount',
      component: CreateAccount,
      meta: { transitionName: 'slide' }
    },

    // Create Listing
    {
      path: '/stays/submit-stay',
      name: 'submitStay',
      component: SubmitStay,
      meta: { transitionName: 'slide' },

      // Route Guard
      // beforeEnter(to, from, next) {
      //   try {
      //     var loggedIn = store.getters.user;
      //     if (loggedIn) {
      //       next()
      //     }
      //   } catch (e) {
      //     next({
      //       name: "Results"
      //     })
      //   }
      // }
    },

    // Account
    {
      path: 'stays/account',
      name: 'ManageAccount',
      component: ManageAccount,
      meta: { transitionName: 'slide' }
    },
    {
      path: 'stays/messages',
      name: 'Messages',
      component: Messages,
      meta: { transitionName: 'slide' }
    },
  ]
}]
