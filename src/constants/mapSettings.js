import { retroMap } from '@/constants/mapStyles'

const mapSettings = {
  clickableIcons: false,
  streetViewControl: false,
  panControlOptions: false,
  gestureHandling: "cooperative",
  mapTypeControl: false,
  zoomControlOptions: {
    style: "SMALL"
  },
  zoom: 12,
  minZoom: 2,
  maxZoom: 20,
  styles: retroMap['style']
};

export { mapSettings }
