import Vue from 'vue'
import Vuex from 'vuex'
import auth from '@/store/modules/auth'
import nav from '@/store/modules/nav'
import results from '@/store/modules/results'
import map from '@/store/modules/map'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    auth,
    nav,
    results,
    map
  }
})
