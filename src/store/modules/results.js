import { DB } from '@/firebase/db'
import { Storage } from '@/firebase/storage'
import Stay from '@/lib/stay'

//-- State --------------------------------------------------------------------
const state = {
  results: []
}

//-- Getters ------------------------------------------------------------------
const getters = {
  results: (state) => state.results
}

//-- Actions ------------------------------------------------------------------
const actions = {
  async loadResults({commit}) {
    try {
      const collection = await DB.collection('stays').get()
      let references = []
      collection.forEach(doc => references.push(doc))
      let results = references.map(stay => new Stay(stay))
      commit('setResults', results)
    } catch(e) {
      alert(e)
    }
  },

  async createListing({dispatch}, form) {
    try {
      const collection = await DB.collection('stays')
      const stay = await collection.add({
        address: form.values.address,
        beds: form.values.beds,
        baths: form.values.baths,
        description: form.values.description,
        price: form.values.price,
        geopoint: form.values.geopoint
      })
      dispatch('uploadImages', {form, stay})
    } catch(e) {
      alert(e)
    }
  },

  async uploadImages({dispatch}, options) {
    const storageRef = Storage.ref()
    const stay = options.stay
    const images = options.form.values.images

    const promises = images.map(async (image, idx) => {
      let imgRef = storageRef.child(`${stay.id}-${idx}.jpg`)
      let newImg = await imgRef.put(image)
      return await newImg.ref.getDownloadURL()
    })

    try {
      const imgURLs = await Promise.all(promises)
      dispatch('setStayImages', {stay, imgURLs})
    } catch(e){
      alert(e)
    }
  },


  async setStayImages({dispatch}, options) {
    const stay = options.stay
    const imgURLs = options.imgURLs
    const stayRef = await DB.collection('stays').doc(stay.id)
    try {
      await stayRef.set({
        images: imgURLs
      }, { merge: true })
      dispatch('loadResults')
      dispatch('createMarkers')
    } catch(e) {
      alert(e)
    }
  }
}

//-- Mutations ----------------------------------------------------------------
const mutations = {
  setResults: (state, results) => (state.results = results),
  setMarkers: (state, markers) => (state.markers = markers)
}

//-- Export -------------------------------------------------------------------
export default {
  state,
  getters,
  actions,
  mutations
}
