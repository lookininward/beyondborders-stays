import { App } from '@/firebase/app'

//-- State --------------------------------------------------------------------
const state = {
  user: null
}

//-- Getters ------------------------------------------------------------------
const getters = {
  user: (state) => state.user
}

//-- Actions ------------------------------------------------------------------
const actions = {
  setAuthWatcher({commit}) {
    App.auth().onAuthStateChanged(user => commit('setUser', user))
  },

  async login({commit}, form) {
    try {
      const response = await App.auth().signInWithEmailAndPassword(
        form.email,
        form.password
      )
      commit('setUser', response.user)
    } catch(e) {
      throw e
    }
  },

  async logout({dispatch, commit}) {
    try {
      await App.auth().signOut()
      commit('setUser', null)
      dispatch('goToRoute', 'login')
    } catch(e) {
      throw e
    }
  },

  async createAccount({commit}, form) {
    try {
      const response = await App.auth().createUserWithEmailAndPassword(
        form.email,
        form.password
      )
      commit('setUser', response.user)
    } catch(e) {
      throw e
    }
  }
}

//-- Mutations ----------------------------------------------------------------
const mutations = {
  setUser: (state, user) => (state.user = user)
}

//-- Export -------------------------------------------------------------------
export default {
  state,
  getters,
  actions,
  mutations
}
