import router from '@/router'

//-- Actions ------------------------------------------------------------------
const actions = {
  goToRoute({commit}, route) {
    commit('goToRoute', route)
  },

  goToListing({commit}, id) {
    if (!id) { return }
    commit('goToListing', id)
  }
}

//-- Mutations ----------------------------------------------------------------
const mutations = {
  goToRoute: (state, route) => (router.push(route)),
  goToListing: (state, id) => (router.push({
    name: 'Listing',
    params: { id }
  }))
}

//-- Export -------------------------------------------------------------------
export default {
  actions,
  mutations
}
