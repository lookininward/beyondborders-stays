/* global google */

//-- State --------------------------------------------------------------------
const state = {
  google: null,
  activeMap: null,
  activeStay: null,
  activeCoordinates: null,
  markers: []
}

//-- Getters ------------------------------------------------------------------
const getters = {
  google: (state) => state.google,
  activeMap: (state) => state.activeMap,
  activeStay: (state) => state.activeStay,
  activeCoordinates: (state) => state.activeCoordinates,
  markers: (state) => state.markers
}

//-- Actions ------------------------------------------------------------------
const actions = {
  setGoogle({commit}, googleMapApi) {
    commit('setGoogle', googleMapApi)
  },

  setActiveMap({commit}, map) {
    commit('setActiveMap', map)
  },

  setActiveStay({commit}, stay) {
    commit('setActiveStay', stay)
  },

  setLocation({state}, position) {
    const lat = position.coords.latitude
    const lng = position.coords.longitude
    const activeMap = state.activeMap
    activeMap.setCenter({lat, lng}) // set map location
    activeMap.setZoom(10) // set map zoom level to location neighbourhood
  },

  async setActiveCoordinates({dispatch, commit, rootState}, address) {
    const activeMap = rootState.map.activeMap
    return new Promise(() => {
      const geocoder = new google.maps.Geocoder()
      geocoder.geocode({ address}, function(results, status) {
        if (status == 'OK') {
          activeMap.setCenter(results[0].geometry.location)
          activeMap.setZoom(15)
          let coordinates = {
            latitude: results[0].geometry.location.lat(),
            longitude: results[0].geometry.location.lng()
          }
          commit('setActiveCoordinates', coordinates)
          dispatch('addTemporaryMarker', coordinates)
        }
        // alert('Geocode was not successful: ' + status)
      })
    })
  },

  clearActiveCoordinates({commit}) {
    commit('setActiveCoordinates', null)
  },

  createMarkers({dispatch, commit, rootState}) {
    const google = rootState.map.google
    const map = rootState.map.activeMap
    const results = rootState.results.results
    let markers = []
    if (results && results.length) {
      markers = results.map(stay => {
        let position = {
          lat: stay.geopoint.latitude,
          lng: stay.geopoint.longitude
        };
        return new google.maps.Marker({
          id: stay.id,
          position,
          map
        }).addListener('click', () => {
          map.setCenter(position)
          map.setZoom(15)
          dispatch('setActiveStay', stay)
          dispatch('goToListing', stay.id)
        }, { passive: true })

      })
    }
    commit('setMarkers', markers)
  },

  addTemporaryMarker({commit, state}, coordinates) {
    const google = state.google
    const map = state.activeMap
    let markers = state.markers
    markers.push(
      new google.maps.Marker({
        position: {
          lat: coordinates.latitude,
          lng: coordinates.longitude
        },
        map
      })
    )
    map.setZoom(15)
    commit('setMarkers', markers)
  },

  removeTemporaryMarker({commit, state}) {
    let markers = state.markers
    const temporaryMarker = markers.find(marker => !marker.id)
    if (temporaryMarker) {
      temporaryMarker.setMap(null)
    }
    markers = markers.filter(marker => marker.id)
    commit('setMarkers', markers)
  }
}

//-- Mutations ----------------------------------------------------------------
const mutations = {
  setGoogle: (state, google) => (state.google = google),
  setActiveMap: (state, map) => (state.activeMap = map),
  setActiveStay: (state, stay) => (state.activeStay = stay),
  setActiveCoordinates: (state, coords) => (state.activeCoordinates = coords),
  setMarkers: (state, markers) => (state.markers = markers)
}

//-- Export -------------------------------------------------------------------
export default {
  state,
  getters,
  actions,
  mutations
}
