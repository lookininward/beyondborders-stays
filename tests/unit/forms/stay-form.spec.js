import StayForm from '@/lib/stayForm'

describe('Form - Submit Stay', () => {
  let form = new StayForm()

  beforeEach(() => {
    form.clearErrors()
  })

  it('clearInput(input)', () => {
    const values = form.values
    values.beds = 2
    values.baths = 2
    form.clearInput('beds')
    expect(values.beds).toBe(null)
    form.clearInput('baths')
    expect(values.baths).toBe(null)
  })

  it('clearErrors()', () => {
    let errors = form.errors
    errors.beds = 'error'
    errors.baths = 'error'
    form.clearErrors()
    errors = form.errors
    expect(errors.beds).toBe(null)
    expect(errors.baths).toBe(null)
  })

  it('clearError(input)', () => {
    let errors = form.errors
    errors.beds = 'error'
    errors.baths = 'error'
    form.clearError('beds')
    expect(errors.beds).toBe(null)
    form.clearError('baths')
    expect(errors.baths).toBe(null)
  })

  it('validateForm()', () => {
    const isValid = form.validateForm()
    expect(isValid).toBe(false)
    let errors = form.errors
    expect(errors.beds).toBe('missing')
    expect(errors.baths).toBe('missing')
    expect(errors.price).toBe('missing')
    expect(errors.address).toBe('missing')
    expect(errors.geopoint).toBe('missing')
  })

  it('validateInput(input)', () => {
    let errors = form.errors
    expect(errors.beds).toBe(null)
    form.validateInput('beds')
    expect(errors.beds).toBe('missing')
  })

  it('addImages(images)', () => {
    const images = form.values.images
    expect(images.length).toBeFalsy()
    form.addImages(['img1', 'img2'])
    expect(images.length).toBe(2)
    form.addImages(['img3', 'img4'])
    expect(images.length).toBe(2)
  })
})
