import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vue from 'vue'
import VueRouter from 'vue-router'
import Vuex from 'vuex'
import Results from '@/views/Results.vue'

const localVue = createLocalVue()
localVue.use(Vuex)
Vue.use(VueRouter)

describe('Results Route', () => {

  //-- Setup ------------------------------------
  let store
  beforeEach(() => {
    store = new Vuex.Store({
      modules: {
        auth: {
          getters: {
            user: jest.fn(),
          }
        }
      }
    })
  })

  it('Results route does render', () => {
    const results = shallowMount(Results, {
      store,
      localVue,
      computed: {
        activeMap() { return true },
        results() { return true }
      }
    })

    // container
    expect(results.attributes()['data-test-route']).toBe('results')
    expect(results.attributes('class')).toBe('results results--showToolbar')

    // search
    expect(results.find('[data-test-input="search"]').exists()).toBe(true)
    expect(results.find('[data-test-btn="toggleToolbar"]').exists()).toBe(true)

    // toolbar
    expect(results.find('[data-test-toolbar="results"]').exists()).toBe(true)
    expect(results.find('[data-test-select="beds"]').exists()).toBe(true)
    expect(results.find('[data-test-select="baths"]').exists()).toBe(true)
    expect(results.find('[data-test-input="min"]').exists()).toBe(true)
    expect(results.find('[data-test-input="max"]').exists()).toBe(true)
    expect(results.find('[data-test-input="furnished"]').exists()).toBe(true)
    expect(results.find('[data-test-select="sort"]').exists()).toBe(true)

    // default data
    expect(results.vm.searchTerm).toBe(null)
    expect(results.vm.showToolbar).toBe(true)
    expect(results.vm.filters).toStrictEqual({
      'min': 1000,
      'max': 3000
    })
  })

  //-- Methods ----------------------------------
  it('searchLocation() called when keyup enter in search', () => {
    const searchLocation = jest.fn()
    const results = shallowMount(Results, {
      store,
      localVue,
      computed: {
        activeMap() { return true },
        results() { return true }
      },
      methods: { searchLocation }
    })

    const search = results.find('[data-test-input="search"]')
    search.trigger('keyup.enter')
    expect(searchLocation).toBeCalled()
  })

})
