import { mount, shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import Login from '@/views/Login.vue'

let localVue = createLocalVue()
localVue.use(Vuex)

describe('Login Route', () => {

  //-- Setup ------------------------------------
  let store
  beforeEach(() => {
    store = new Vuex.Store({
      modules: {
        auth: {
          getters: {
            user: function() {
              return {
                email: 'test@test.com'
              }
            }
          }
        }
      },
      actions: {
        login: jest.fn(),
        logout: jest.fn()
      }
    })
  })

  //-- DOM --------------------------------------
  it('Login route does render', () => {
    let createAccount = shallowMount(Login, { store, localVue })
    expect(
      createAccount.attributes()['data-test-route']
    ).toBe('login')
  })

  it('Renders correct component based on state', () => {
    let login = mount(Login, { store, localVue })
    expect(
      login.find('[data-test-component="login-auth"]').exists()
    ).toBe(true)
  })
})

// Route: Login
// if logged in, transition to manage-account
// if not logged in, display login form
// login form validation
// if successful login, transition to manage-account
// if fail, show generic error
