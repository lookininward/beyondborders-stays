import { mount, shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import SubmitStay from '@/views/SubmitStay.vue'
import StayForm from '@/lib/stayForm'

let localVue = createLocalVue()
localVue.use(Vuex)

import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

describe('SubmitStay Route', () => {

  //-- Setup ------------------------------------
  let store
  beforeEach(() => {
    store = new Vuex.Store({
      modules: {
        auth: {
          getters: {
            user: jest.fn()
          }
        },
        map: {
          getters: {
            activeCoordinates: jest.fn()
          }
        }
      }
    })
  })

  //-- DOM --------------------------------------
  it('SubmitStay route does render', () => {
    let submitStay = shallowMount(SubmitStay, { store, localVue })
    expect(submitStay.attributes()['data-test-route']).toBe('submit')
  })

  it('SubmitStay renders correct component based on state', () => {
    /*
      States:
        Auth
        Info
        Images
        Description
        PreviewSubmit
        Success
    */

    //-- Auth --
    let submitStay = mount(SubmitStay, { store, localVue })
    expect(submitStay.find('[data-test-step="Auth"]').exists()).toBe(true)

    //-- Create --
    submitStay = mount(SubmitStay, { store, localVue,
      computed: {
        user() { return true }
      }
    })
    expect(submitStay.find('[data-test-step="Info"]').exists()).toBe(true)

    //-- Upload Images --
    submitStay.vm.state = 'Images'
    expect(submitStay.find('[data-test-step="Images"]').exists()).toBe(true)

    //-- Add Description --
    submitStay.vm.state = 'Description'
    expect(
      submitStay.find('[data-test-step="Description"]').exists()
    ).toBe(true)

    //-- Preview Submit --
    submitStay.vm.state = 'Preview'
    expect(submitStay.find('[data-test-step="Preview"]').exists()).toBe(true)

    //-- Success --
    // submitStay.vm.state = 'success'
    // expect(submitStay.find('[data-test-step="success"]').exists()).toBe(true)
  })

  //-- Data -------------------------------------
  it('StayForm created and set', () => {
    let submitStay = shallowMount(SubmitStay, { store, localVue })
    expect(submitStay.vm.form).toStrictEqual(new StayForm())
  })

  //-- Computed ---------------------------------
  it('state returns state string based on user', () => {
    let submitStay = shallowMount(SubmitStay, { store, localVue })
    expect(submitStay.vm.state).toBe('Auth')

    submitStay = shallowMount(SubmitStay, {
      store,
      localVue,
      computed: {
        user() { return true }
      }
    })
    expect(submitStay.vm.state).toBe('Info')
  })
})

//-- Authentication ---------------------------
// must be authenticated to enter route

// it('must be authenticated to enter Submit route', () => {
//   const next = jest.fn()
//   let submitStay = shallowMount(SubmitStay, {
//     store,
//     localVue,
//     mocks: {
//       $router: { next }
//     }
//   })
//   expect(next).toBeCalled()
// })
