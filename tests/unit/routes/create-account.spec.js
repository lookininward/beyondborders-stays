import { mount, shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import CreateAccount from '@/views/CreateAccount.vue'

let localVue = createLocalVue()
localVue.use(Vuex)

describe('CreateAccount Route', () => {

  //-- Setup ------------------------------------
  let store
  beforeEach(() => {
    store = new Vuex.Store({
      modules: {
        auth: {
          getters: {
            user: jest.fn()
          }
        }
      }
    })
  })

  //-- DOM --------------------------------------
  it('CreateAccount route does render', () => {
    let createAccount = mount(CreateAccount, {store, localVue})
    expect(
      createAccount.attributes()['data-test-route']
    ).toBe('create-account')
    expect(
      createAccount.find('[data-test-component="account-create"]').exists()
    ).toBe(true)
  })

  // clicking create account btn runs method tryCreateAccount()

  //-- Methods ----------------------------------
  // createAccount() calls create account in vuex store
  // _clearErrors() sets resets errors object
  // _validateForm() valids email, password field and returns !hasErrors
  // _setServerError() sets the correct error message
  // tryCreateAccount() calls _clearErrors(), _validateForm(), createAccount(), sets and unsets loading state, sets server error on fail

  it('_setServerError() sets the correct error message', () => {
    let createAccount = shallowMount(CreateAccount, { store, localVue })
    expect(createAccount.vm.errors.server).toBe(null)
    const codes = {
      'auth/invalid-email' : 'Invalid email',
      'auth/email-already-in-use' : 'The email address is already in use by another account.',
      'auth/weak-password': 'Password should be at least 6 characters'
    }
    const keys = Object.keys(codes)
    keys.forEach(key => {
      createAccount.vm._setServerError(key)
      expect(createAccount.vm.errors.server).toBe(codes[key])
    })
  })
})

// if account created show success state, then transition to manage-account
