import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import ManageAccount from '@/views/ManageAccount.vue'

let localVue = createLocalVue()
localVue.use(Vuex)

describe('ManageAccount Route', () => {

  //-- Setup ------------------------------------
  let store
  beforeEach(() => {
    store = new Vuex.Store({
      modules: {
        auth: {
          getters: {
            user: function() {
              return {
                email: 'test@test.com'
              }
            }
          }
        },
        results: {
          getters: {
            results: function() {
              return []
            }
          }
        }
      }
    })
  })

  //-- DOM --------------------------------------
  it('ManageAccount route does render', () => {
    const push = jest.fn()
    let manageAccount = shallowMount(ManageAccount, {
      store,
      localVue,
      mocks: {
        $router: { push }
      }
    })
    expect(
      manageAccount.attributes()['data-test-route']
    ).toBe('account-manage')
  })
})
