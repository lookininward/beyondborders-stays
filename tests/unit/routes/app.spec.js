import { shallowMount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'
import App from '@/App.vue'

import Vuex from 'vuex'
let localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('App Route', () => {
  let store
  const setAuthWatcher = jest.fn()
  const setGoogle = jest.fn()
  beforeEach(() => {
    store = new Vuex.Store({
      actions: {
        setAuthWatcher,
        setGoogle
      }
    })
  })

  it('app route does render', () => {
    const app = shallowMount(App, { store, localVue })
    expect(app.attributes()['data-test-app']).toBe('beyondborders-stays')
  })

  it('sets auth state watcher', () => {
    expect(setAuthWatcher).toBeCalled()
  })

  // it('sets google maps api', () => {
  //   expect(setGoogle).toBeCalled()
  // })
})
