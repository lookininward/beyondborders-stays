import { shallowMount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'
import auth from '@/store/modules/auth'

import Vuex from 'vuex'
let localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('App Route', () => {
  const setAuthWatcher = jest.fn()
  let store
  beforeEach(() => {
    store = new Vuex.Store({
      modules: {
        auth
      },
      actions: {
        setAuthWatcher
      }
    })
  })

  it('sets user on onAuthStateChanged', () => {
    // expect(store.state.user).toBeFalsy()
    // store.dispatch('setAuthWatcher')
    expect(true)
  })

  // Getters
  // user

  // Actions
  // login
  // logout
  // createAccount

  // Mutations
  // set user
})
