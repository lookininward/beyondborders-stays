import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import Navigation from '@/components/Navigation.vue'
let localVue = createLocalVue()
localVue.use(Vuex)

describe('Component - Navigation', () => {

  //-- Setup ------------------------------------
  const $route = {
    name: 'stays',
    path: '/'
  }

  let store
  beforeEach(() => {
    store = new Vuex.Store({
      modules: {
        auth: {
          getters: {
            user: jest.fn()
          }
        }
      },
      actions: {
        login: jest.fn(),
        logout: jest.fn()
      }
    })
  })

  //-- DOM --------------------------------------
  it('component does render', () => {
    /*
      Nav renders with the elements:
        - logo
        - search input
        - actions btns
          - submit post
          - login
    */
    const nav = shallowMount(Navigation, {
      store,
      localVue,
      mocks: { $route }
    })
    expect(nav.attributes()['data-test-component']).toBe('nav')
    expect(nav.find('[data-test-logo]').exists()).toBe(true)
    expect(nav.find('[data-test-input="search"]').exists()).toBe(true)
    expect(nav.find('[data-test-btn="submitStay"]').exists()).toBe(true)
    expect(nav.find('[data-test-btn="login"]').exists()).toBe(true)
  })

  it('clicking logo calls goHome()', () => {
    const goToRoute = jest.fn()
    const nav = shallowMount(Navigation, {
      store,
      localVue,
      mocks: { $route },
      methods: { goToRoute }
    })
    const logo = nav.find('[data-test-logo]')
    logo.trigger('click')
    expect(goToRoute).toBeCalled()
  })

  it('submit stay button calls goTo("stays/submit-stay")', () => {
    const goToRoute = jest.fn()
    const nav = shallowMount(Navigation, {
      store,
      localVue,
      mocks: { $route },
      methods: { goToRoute }
    })
    const submitStay = nav.find('[data-test-btn="submitStay"]')
    submitStay.trigger('click')
    expect(goToRoute).toBeCalledWith('/stays/submit-stay')
  })

  it('login button calls goTo("stays/login")', () => {
    const goToRoute = jest.fn()
    const nav = shallowMount(Navigation, {
      store,
      localVue,
      mocks: { $route },
      methods: { goToRoute }
    })
    const login = nav.find('[data-test-btn="login"]')
    login.trigger('click')
    expect(goToRoute).toBeCalledWith('/stays/login')
  })

  //-- Computed ---------------------------------
  it('searchPlaceholder returns string based on the current route', () => {
    let nav = shallowMount(Navigation, {
      store,
      localVue,
      mocks: { $route }
    })
    expect(nav.vm.searchPlaceholder).toBe('Need a place to rest your head?')
    nav = shallowMount(Navigation, {
      store,
      localVue,
      mocks: {
        $route: {
          name: 'meow',
          path: '/meow'
        }
      }
    })
    expect(nav.vm.searchPlaceholder).toBe('')
  })

  //-- Methods ----------------------------------
  it('goToRoute(route) transitions to the chosen route', () => {
    const goToRoute = jest.fn()
    let nav = shallowMount(Navigation, {
      store,
      localVue,
      mocks: {
        $route
      },
      methods: {
        goToRoute
      }
    })
    nav.vm.goToRoute('/meow')
    expect(goToRoute).toBeCalledWith('/meow')
  })
})
