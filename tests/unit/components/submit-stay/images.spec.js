import { shallowMount } from '@vue/test-utils'
import Images from '@/components/submit/Images.vue'
import StayForm from '@/lib/stayForm'

describe('SubmitStay - Images component', () => {
  const form = new StayForm

  beforeEach(() => {
    form.values.gallery = []
  })

  it('SubmitStay - Images component does render', () => {
    const images = shallowMount(Images, {
      propsData: {
        form,
        goBack: jest.fn(),
        goToStep: jest.fn()
      }
    })
    expect(images.attributes()['data-test-step']).toBe('Images')
  })

  it('If existing gallery, initializeCroppie and addToCroppie', () => {
    const initializeCroppie = jest.fn()
    const addToCroppie = jest.fn()
    form.values.gallery = ['img1', 'img2']
    const images = shallowMount(Images, {
      propsData: {
        form,
        goBack: jest.fn(),
        goToStep: jest.fn()
      },
      methods: {
        initializeCroppie,
        addToCroppie
      }
    })
    expect(initializeCroppie).toBeCalled()
    expect(addToCroppie).toBeCalled()
  })

  //-- Computed ---------------------------------
  it('allCropped', () => {
    const initializeCroppie = jest.fn()
    const addToCroppie = jest.fn()
    form.values.gallery = [{ croppedFile: null }, { croppedFile: null }]
    let images = shallowMount(Images, {
      propsData: {
        form,
        goBack: jest.fn(),
        goToStep: jest.fn()
      },
      methods: {
        initializeCroppie,
        addToCroppie
      }
    })
    expect(images.vm.allCropped).toBeFalsy()

    form.values.gallery = [{ croppedFile: 'file1' }, { croppedFile: null }]
    images = shallowMount(Images, {
      propsData: {
        form,
        goBack: jest.fn(),
        goToStep: jest.fn()
      },
      methods: {
        initializeCroppie,
        addToCroppie
      }
    })
    expect(images.vm.allCropped).toBeFalsy()

    form.values.gallery = [{ croppedFile: 'file1' }, { croppedFile: 'file2' }]
    images = shallowMount(Images, {
      propsData: {
        form,
        goBack: jest.fn(),
        goToStep: jest.fn()
      },
      methods: {
        initializeCroppie,
        addToCroppie
      }
    })
    expect(images.vm.allCropped).toBe(true)
  })

  //-- Methods ----------------------------------
  it('addFiles(e) - uploadZone', () => {
    const addFiles = jest.fn()
    const images = shallowMount(Images, {
      propsData: {
        form,
        goBack: jest.fn(),
        goToStep: jest.fn()
      },
      methods: {
        addFiles
      }
    })
    const uploadZone = images.find('[data-test-input="uploadZone')
    uploadZone.trigger('drop')
    expect(addFiles).toBeCalled()
  })

  it('addFiles(e) - images input', () => {
    const addFiles = jest.fn()
    const images = shallowMount(Images, {
      propsData: {
        form,
        goBack: jest.fn(),
        goToStep: jest.fn()
      },
      methods: {
        addFiles
      }
    })
    const imagesInput = images.find('[data-test-input="images')
    imagesInput.trigger('change')
    expect(addFiles).toBeCalled()
  })

  // handleFiles
  // addToGallery
  // initializeCroppie
  // addToCroppie
  // cropImage
  // goToAddDescription
})
