import { shallowMount } from '@vue/test-utils'
import Auth from '@/components/submit/Auth.vue'

describe('SubmitStay - Auth component', () => {

  it('Submit - Auth component does render', () => {
    const auth = shallowMount(Auth)
    expect(auth.attributes()['data-test-step']).toBe('Auth')
    expect(auth.find('[data-test-btn="login"]').exists()).toBe(true)
    expect(auth.find('[data-test-btn="createAccount"]').exists()).toBe(true)
  })

  it('Login button calls goTo("/stays/login")', () => {
    const goTo = jest.fn()
    const auth = shallowMount(Auth, { methods: { goTo } })
    const login = auth.find('[data-test-btn="login"]')
    login.trigger('click')
    expect(goTo).toBeCalledWith('/stays/login')
  })

  it('Create Account button calls goTo("/stays/create-account")', () => {
    const goTo = jest.fn()
    const auth = shallowMount(Auth, { methods: { goTo } })
    const createAccount = auth.find('[data-test-btn="createAccount"]')
    createAccount.trigger('click')
    expect(goTo).toBeCalledWith('/stays/create-account')
  })
})
