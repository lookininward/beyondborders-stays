import { shallowMount } from '@vue/test-utils'
import Preview from '@/components/submit/Preview.vue'
import StayForm from '@/lib/stayForm'

describe('SubmitStay - Preview component', () => {
  const form = new StayForm

  it('SubmitStay - Preview component does render', () => {
    const submitListing = jest.fn()
    const preview = shallowMount(Preview, {
      propsData: {
        form,
        goBack: jest.fn(),
        submitListing,
        preview: {}
      }
    })
    expect(preview.attributes()['data-test-step']).toBe('Preview')
    const submitListingBtn = preview.find('[data-test-btn="submitListing"]')
    submitListingBtn.trigger('click')
    expect(submitListing).toBeCalled()
  })
})
