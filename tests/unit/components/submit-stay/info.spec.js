import { shallowMount, createLocalVue } from '@vue/test-utils'
import Info from '@/components/submit/Info.vue'
import StayForm from '@/lib/stayForm'

import Vuex from 'vuex'
let localVue = createLocalVue()
localVue.use(Vuex)

import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

describe('SubmitStay - Info component', () => {
  const form = new StayForm

  let store
  beforeEach(() => {
    store = new Vuex.Store({
      modules: {
        auth: {
          getters: {
            user: jest.fn()
          }
        },
        map: {
          getters: {
            activeCoordinates: jest.fn()
          }
        }
      }
    })
  })

  it('SubmitStay - Info component does render', () => {
    const info = shallowMount(Info, { store, localVue,
      propsData: {
        form,
        goToStep: jest.fn()
      }
    })
    expect(info.attributes()['data-test-step']).toBe('Info')
  })

  it('focus address input - calls focusAddress', () => {
    const focusAddress = jest.fn()
    const info = shallowMount(Info, { store, localVue,
      propsData: {
        form,
        goToStep: jest.fn(),
      },
      methods: {
        focusAddress
      }
    })
    const addressInput = info.find('[data-test-input="search"]')
    addressInput.trigger('focus')
    expect(focusAddress).toBeCalled()
  })

  it('focus price input - calls focusPrice', () => {
    const focusPrice = jest.fn()
    const info = shallowMount(Info, { store, localVue,
      propsData: {
        form,
        goToStep: jest.fn(),
      },
      methods: {
        focusPrice
      }
    })
    const priceInput = info.find('[data-test-input="price"]')
    priceInput.trigger('focus')
    expect(focusPrice).toBeCalled()
  })

  it('search location - calls checkCoordinates', () => {
    const checkCoordinates = jest.fn()
    const info = shallowMount(Info, { store, localVue,
      propsData: {
        form,
        goToStep: jest.fn(),
      },
      methods: {
        checkCoordinates
      }
    })
    const searchBtn = info.find('[data-test-btn="checkCoordinates"]')
    searchBtn.trigger('click')
    expect(checkCoordinates).toBeCalled()
  })

  it('click furnished input - calls toggleFurnished', () => {
    const toggleFurnished = jest.fn()
    const info = shallowMount(Info, { store, localVue,
      propsData: {
        form,
        goToStep: jest.fn(),
      },
      methods: {
        toggleFurnished
      }
    })
    const furnishedInput = info.find('[data-test-input="furnished"]')
    furnishedInput.trigger('click')
    expect(toggleFurnished).toBeCalled()
  })
})
