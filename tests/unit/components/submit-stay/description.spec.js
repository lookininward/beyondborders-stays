import { shallowMount } from '@vue/test-utils'
import Description from '@/components/submit/Description.vue'
import StayForm from '@/lib/stayForm'

describe('SubmitStay - Description component', () => {
  const form = new StayForm

  it('SubmitStay - Description component does render', () => {
    const description = shallowMount(Description, {
      propsData: {
        form,
        goBack: jest.fn(),
        goToStep: jest.fn()
      }
    })
    expect(description.attributes()['data-test-step']).toBe('Description')
  })
})
