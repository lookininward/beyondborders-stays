import { shallowMount } from '@vue/test-utils'
import Success from '@/components/login/Success.vue'

describe('Login - Success component', () => {

  //-- DOM --------------------------------------
  it('Login - Success component does render', () => {
    let login = shallowMount(Success, {})
    expect(login.attributes()['data-test-component']).toBe('login-success')
  })
})
