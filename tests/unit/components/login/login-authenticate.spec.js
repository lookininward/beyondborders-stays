import { shallowMount } from '@vue/test-utils'
import Authenticate from '@/components/login/Authenticate.vue'

describe('Login - Authenticate component', () => {

  //-- DOM --------------------------------------
  it('Login - Authenticate component does render', () => {
    let auth = shallowMount(Authenticate, {
      propsData: {
        form: {},
        errors: {},
        attemptLogin: jest.fn()
      },
    })
    expect(auth.attributes()['data-test-component']).toBe('login-auth')
    expect(auth.find('[data-test-btn="attemptLogin"]').exists()).toBe(true)
    expect(auth.find('[data-test-input="email"]').exists()).toBe(true)
    expect(auth.find('[data-test-input="password"]').exists()).toBe(true)
  })

  it('clicking login button calls attemptLogin()', () => {
    const attemptLogin = jest.fn()
    const auth = shallowMount(Authenticate, {
      propsData: {
        form: {},
        errors: {},
        attemptLogin
      }
    })
    const login = auth.find('[data-test-btn="attemptLogin"]')
    login.trigger('click')
    expect(attemptLogin).toBeCalled()
  })
})
