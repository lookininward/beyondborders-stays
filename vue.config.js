module.exports = {
  css: {
    loaderOptions: {
      sass: {
        data: `
          @import "@/scss/_variables.scss";
          @import "@/scss/_mixins.scss";
          @import "@/scss/_buttons.scss";
          @import "@/scss/_text.scss";
        `
      }
    }
  },

  devServer: {
    disableHostCheck: true
  }
};

// @import "../node_modules/croppie/croppie.css";
